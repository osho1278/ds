root=None
class Node:
	def __init__(self,data=None):
		self.left = None
		self.right = None
		self.data=data
def insert(root,data):
	if(root==None):
		root = Node(data)
	elif(root.data>data):
		if(root.left!=None):
			insert(root.left,data)
		else:
			root.left=Node(data)
	else:
		if(root.right!=None):
			insert(root.right,data)
		else:
			root.right=Node(data)
def Inorder(root):
	if(root==None):
		return
	Inorder(root.left)
	print root.data
	Inorder(root.right)

	#return root	
arr=[9,8,7,6,5,4,3,2,1,0]

for i in xrange(len(arr)):
	if(i==0):
		root= Node(arr[i])
	else:
		insert(root,arr[i])
print root.data
Inorder(root)