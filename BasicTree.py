class Node:
	def __init__(self,data):
		self.left = None
		self.right = None
		self.data=data


root = Node(10)

left =root.left

root.left = Node(20)
root.right = Node(30)

print root.data
print left
print root.left
print root.right.data
